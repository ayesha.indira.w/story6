from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Activity, Member

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.activity_url = reverse("story6:activity")
        self.test_activity = Activity.objects.create(
            activity = "tidur"
        )
        self.test_member = Member.objects.create(
            member = "indi",
            aktivitas = self.test_activity
        )
        self.test_delete_url = reverse("story6:activityDelete",args=[self.test_activity.id])
        self.test_member_delete_url = reverse("story6:memberDelete",args=[self.test_member.id])

    def test_activity_GET(self):
        response = self.client.get(self.activity_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "templates/index.html")

    def test_activity_POST(self):
        response = self.client.post(self.activity_url, {
            "activity" : "nonton"
        }, follow=True)
        self.assertContains(response, "nonton")

    def test_member_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "ayes",
            "aktivitas" : self.test_activity.id,
        }, follow=True)
        self.assertContains(response, "ayes")

    def test_notValid_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "ayes",
            "aktivitas" : "apaya",
        }, follow=True)
        self.assertContains(response, "Invalid input")

    def test_member_DELETE(self):
        response = self.client.get(self.test_member_delete_url, follow=True)
        self.assertContains(response, " sucessfully deleted")

    def test_activity_DELETE(self):
        response = self.client.get(self.test_delete_url, follow=True)
        self.assertContains(response, " sucessfully deleted")
